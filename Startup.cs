﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HizmetTalep.Startup))]
namespace HizmetTalep
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

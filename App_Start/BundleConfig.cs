﻿ using System.Web;
using System.Web.Optimization;

namespace HizmetTalep
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/jquery-1.11.1.js",
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/hover.zoom.conf.js",
                        "~/Scripts/hover.zoom.js",
                        "~/Scripts/modernizr-*"));
            bundles.Add(new ScriptBundle("~/Scripts/JSAnasayfa").Include(
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/hover.zoom.conf.js",
                        "~/Scripts/hover.zoom.js"));
            bundles.Add(new ScriptBundle("~/Scripts/JSAdmin").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/jquery-1.11.1.js"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                    
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/CssAdmin").Include(
                      "~/Content/CssAdmin/style.css",
                      "~/Content/CssAdmin/font-awesome.css",
                      "~/Content/CssAdmin/bootstrap.css",
                      "~/Content/Css/CssAdmin/main.css"));

            bundles.Add(new StyleBundle("~/Content/CssAnasayfa").Include(
                     "~/Content/CssAnasayfa/bootstrap.css",
                     "~/Content/CssAnasayfa/main.css"));

        }
    }
}

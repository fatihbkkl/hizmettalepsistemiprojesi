//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HizmetTalep.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Duyurular
    {
        public int Duyuru_ID { get; set; }
        [Display(Name = "Duyuru Tarihi")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime Duyuru_Tarih { get; set; }
        [Display(Name = "Duyuru A��klama")]
        public string Duyuru_Aciklama { get; set; }
    }
}

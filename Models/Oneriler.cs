//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HizmetTalep.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class Oneriler
    {
        public int Oneri_ID { get; set; }
        [Display(Name = "Mail Adres")]
        [EmailAddress(ErrorMessage = "Ge�erli bir mail adresi giriniz.")]
        public string OneriYapan_Mail { get; set; }
        [Display(Name = "Tarih")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime OneriYapim_Tarihi { get; set; }
        [Display(Name = "�neri A��klamas�")]
        public string Oneri_Aciklama { get; set; }
    }
}

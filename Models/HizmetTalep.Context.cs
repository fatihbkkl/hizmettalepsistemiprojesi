﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HizmetTalep.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class HizmetTalepEntities : DbContext
    {
        public HizmetTalepEntities()
            : base("name=HizmetTalepEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Duyurular> Duyurular { get; set; }
        public virtual DbSet<HizmetTur_Tipi> HizmetTur_Tipi { get; set; }
        public virtual DbSet<HizmetVeren> HizmetVeren { get; set; }
        public virtual DbSet<IstenenHizmet> IstenenHizmet { get; set; }
        public virtual DbSet<Kullanicilar> Kullanicilar { get; set; }
        public virtual DbSet<Oneriler> Oneriler { get; set; }
        public virtual DbSet<Sikayetler> Sikayetler { get; set; }
        public virtual DbSet<Yetkilendirme> Yetkilendirme { get; set; }
    }
}

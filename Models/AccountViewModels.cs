﻿using System.ComponentModel.DataAnnotations;

namespace HizmetTalep.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class ManageUserViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Sistem Kullanıcı Adı")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        //[Display(Name = "Sistem Şifre")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Kullanıcı Adı*")]
        [StringLength(50, ErrorMessage = "{0} en az {2} uzunlukta olmalı.", MinimumLength = 6)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "{0} en az {2} uzunlukta olmalı.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        //[Display(Name = "Şifre*")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        //[Display(Name = "Şifre(Tekrar)*")]
        [Compare("Password", ErrorMessage = "Şifreleriniz Eşleşmiyor.")]
        public string ConfirmPassword { get; set; }

        [Required]
        //[Display(Name = "Adınız*")]
        [StringLength(50, ErrorMessage = "{0} en az {2} uzunlukta olmalı.", MinimumLength = 2)]
        public string Kullanici_Adi { get; set; }

        [Required]
        //[Display(Name = "Soyadınız*")]
        [StringLength(50, ErrorMessage = "{0} en az {2} uzunlukta olmalı.", MinimumLength = 2)]
        public string Kullanici_Soyadi { get; set; }

        [Required]
        //[Display(Name = "Mail Adresiniz*")]
        [EmailAddress(ErrorMessage="Geçerli bir mail adresi giriniz.")]
        public string Kullanici_Mail { get; set; }

        [Required]
        //[Display(Name = "Ev Adresiniz*")]
        [StringLength(50, ErrorMessage = "{0} en az {2} uzunlukta olmalı.", MinimumLength = 3)]
        public string Kullanici_Adres { get; set; }

        [Required]
        //[Display(Name = "Telefon Numaranız*")]
        [StringLength(11, ErrorMessage = "{0} en az {2} uzunlukta olmalı.", MinimumLength = 11)]
        public string Kullanici_Tel { get; set; }
    }
}

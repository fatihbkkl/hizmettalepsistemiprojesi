//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HizmetTalep.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class IstenenHizmet
    {
        public int IstenenHizmet_ID { get; set; }

        [Display(Name = "�stenilen Hizmet")]
        public int IstenenHizmet_Tipi { get; set; }

        [Display(Name = "Ad ve Soyad")]
        public string HizmetiIsteyen_KisiAdSoyad { get; set; }

        [Display(Name = "Telefon Numaras�")]
        public string HizmetiIsteyen_KisiTelefon { get; set; }

        [Display(Name = "Talep Tarihi")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime IstenenHizmet_TalepTarihi { get; set; }
        [Display(Name = "�stenen Hizmet")]
        public virtual HizmetTur_Tipi HizmetTur_Tipi { get; set; }
    }
}

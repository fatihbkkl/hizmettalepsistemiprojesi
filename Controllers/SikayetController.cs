﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HizmetTalep.Models;

namespace HizmetTalep.Controllers
{
    
    public class SikayetController : Controller
    {//kullanıcı olayları tamamdır.
        private HizmetTalepEntities db = new HizmetTalepEntities();
        

        // GET: /Sikayet/
        public ActionResult Index()
        {
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.//admin olayın bakmadın.

                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (Convert.ToInt32(Session["GirisYapanYetki"])==2)
                {//müsteri ise
                    return RedirectToAction("Index", "Home");
                }
                else
                {//admin ise
                    return RedirectToAction("Index", "Home");
                }

                //return View(db.Sikayetler.ToList());
            }
            
        }

        // GET: /Sikayet/Details/5
        public ActionResult Details(int? id)
        {
            /*if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.//admin olayın bakmadın.

                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (id == null)
                {//direk url erişmeye kalkarsa bad request ver.
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Sikayetler sikayetler = db.Sikayetler.Find(id);
                if (sikayetler == null)
                {
                    return HttpNotFound();
                }
                return View(sikayetler);
            }
            */
            return RedirectToAction("Index", "Home");
           
        }

        // GET: /Sikayet/Create
        public ActionResult Create()
        {
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
            
        }

        // POST: /Sikayet/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Sikayet_ID,SikayetYapan_Mail,Sikayet_Tarihi,Sikayet_Aciklama")] Sikayetler sikayetler)
        {
            if (ModelState.IsValid)
            {
                db.Sikayetler.Add(sikayetler);
                db.SaveChanges();

                return RedirectToAction("Index","Home");
            }

            return View(sikayetler);
        }

        // GET: /Sikayet/Edit/5
        public ActionResult Edit(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sikayetler sikayetler = db.Sikayetler.Find(id);
            if (sikayetler == null)
            {
                return HttpNotFound();
            }
            return View(sikayetler);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Sikayet/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Sikayet_ID,SikayetYapan_Mail,Sikayet_Tarihi,Sikayet_Aciklama")] Sikayetler sikayetler)
        {
           /* if (ModelState.IsValid)
            {
                db.Entry(sikayetler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sikayetler);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Sikayet/Delete/5
        public ActionResult Delete(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sikayetler sikayetler = db.Sikayetler.Find(id);
            if (sikayetler == null)
            {
                return HttpNotFound();
            }
            return View(sikayetler);
             * */
            return RedirectToAction("Index", "Home");
        }

        // POST: /Sikayet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Sikayetler sikayetler = db.Sikayetler.Find(id);
            db.Sikayetler.Remove(sikayetler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HizmetTalep.Models;

namespace HizmetTalep.Controllers
{
    public class OneriController : Controller
    {//Kullanıcı Girisi olayi tamamen tamamlandi.
        private HizmetTalepEntities db = new HizmetTalepEntities();

        // GET: /Oneri/
        public ActionResult Index()
        {
            /*
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.//admin olayın bakmadın.

                return RedirectToAction("Login", "Account");
            }
            else
            {
                if (Convert.ToInt32(Session["GirisYapanYetki"])==2)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                //return View(db.Oneriler.ToList());
            }

            */
            return RedirectToAction("Index", "Home");
        }

        // GET: /Oneri/Details/5
        public ActionResult Details(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oneriler oneriler = db.Oneriler.Find(id);
            if (oneriler == null)
            {
                return HttpNotFound();
            }
            return View(oneriler);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Oneri/Create
        public ActionResult Create()
        {
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.
                return RedirectToAction("Login", "Account");
            }
            else
            {//kullanici giris yapip hizmet isteye bastiysa buraya gelir.
                //ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi");
                return View();
            }
        }

        // POST: /Oneri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Oneri_ID,OneriYapan_Mail,OneriYapim_Tarihi,Oneri_Aciklama")] Oneriler oneriler)
        {
            if (ModelState.IsValid)
            {
                db.Oneriler.Add(oneriler);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            return View(oneriler);
        }

        // GET: /Oneri/Edit/5
        public ActionResult Edit(int? id)
        {
            /*
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oneriler oneriler = db.Oneriler.Find(id);
            if (oneriler == null)
            {
                return HttpNotFound();
            }
            return View(oneriler);*/
            return RedirectToAction("Index", "Home");   
        }

        // POST: /Oneri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Oneri_ID,OneriYapan_Mail,OneriYapim_Tarihi,Oneri_Aciklama")] Oneriler oneriler)
        {
            /*if (ModelState.IsValid)
            {
                db.Entry(oneriler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oneriler);*/
            return RedirectToAction("Index", "Home");

        }

        // GET: /Oneri/Delete/5
        public ActionResult Delete(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oneriler oneriler = db.Oneriler.Find(id);
            if (oneriler == null)
            {
                return HttpNotFound();
            }
            return View(oneriler);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Oneri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            /*Oneriler oneriler = db.Oneriler.Find(id);
            db.Oneriler.Remove(oneriler);
            db.SaveChanges();*/
            return RedirectToAction("Index","Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

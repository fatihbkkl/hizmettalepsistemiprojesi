﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HizmetTalep.Models;

namespace HizmetTalep.Controllers
{
    public class HizmetController : Controller
    {
        private HizmetTalepEntities db = new HizmetTalepEntities();

        // GET: /Hizmet/
        public ActionResult Index()
        {
            /*if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.
                return RedirectToAction("Login", "Account");
            }
            else
            {
                var istenenhizmet = db.IstenenHizmet.Include(i => i.HizmetTur_Tipi);
                return View(istenenhizmet.ToList());
            }*/
            return RedirectToAction("Index", "Home");

        }

        // GET: /Hizmet/Details/5
        public ActionResult Details(int? id)
        {
           /* if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IstenenHizmet istenenhizmet = db.IstenenHizmet.Find(id);
            if (istenenhizmet == null)
            {
                return HttpNotFound();
            }
            return View(istenenhizmet);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Hizmet/Create
        public ActionResult Create()
        {
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.
                return RedirectToAction("Login", "Account");
            }
            else
            {//kullanici giris yapip hizmet isteye bastiysa buraya gelir.
                ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi");
                return View();
            }
            
        }

        // POST: /Hizmet/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="IstenenHizmet_ID,IstenenHizmet_Tipi,HizmetiIsteyen_KisiAdSoyad,HizmetiIsteyen_KisiTelefon,IstenenHizmet_TalepTarihi")] IstenenHizmet istenenhizmet)
        {
            if (ModelState.IsValid)
            {
                db.IstenenHizmet.Add(istenenhizmet);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }

            ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi", istenenhizmet.IstenenHizmet_Tipi);
            return View(istenenhizmet);
        }

        // GET: /Hizmet/Edit/5
        public ActionResult Edit(int? id)
        {
           /* if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IstenenHizmet istenenhizmet = db.IstenenHizmet.Find(id);
            if (istenenhizmet == null)
            {
                return HttpNotFound();
            }
            ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi", istenenhizmet.IstenenHizmet_Tipi);
            return View(istenenhizmet);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Hizmet/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="IstenenHizmet_ID,IstenenHizmet_Tipi,HizmetiIsteyen_KisiAdSoyad,HizmetiIsteyen_KisiTelefon,IstenenHizmet_TalepTarihi")] IstenenHizmet istenenhizmet)
        {
            /*if (ModelState.IsValid)
            {
                db.Entry(istenenhizmet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi", istenenhizmet.IstenenHizmet_Tipi);
            return View(istenenhizmet);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Hizmet/Delete/5
        public ActionResult Delete(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IstenenHizmet istenenhizmet = db.IstenenHizmet.Find(id);
            if (istenenhizmet == null)
            {
                return HttpNotFound();
            }
            return View(istenenhizmet);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Hizmet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           /* IstenenHizmet istenenhizmet = db.IstenenHizmet.Find(id);
            db.IstenenHizmet.Remove(istenenhizmet);
            db.SaveChanges();
            return RedirectToAction("Index");*/
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

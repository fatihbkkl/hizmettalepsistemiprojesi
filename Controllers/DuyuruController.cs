﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HizmetTalep.Models;

namespace HizmetTalep.Controllers
{
    public class DuyuruController : Controller
    {
        private HizmetTalepEntities db = new HizmetTalepEntities();

        // GET: /Duyuru/
        public ActionResult Index()
        {
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View(db.Duyurular.ToList());
            }
        }

        // GET: /Duyuru/Details/5
        public ActionResult Details(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyurular duyurular = db.Duyurular.Find(id);
            if (duyurular == null)
            {
                return HttpNotFound();
            }
            return View(duyurular);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Duyuru/Create
        public ActionResult Create()
        {
            if (Session["Durum"] == null)
            {//kullanici giris yapmadiysa.
                return RedirectToAction("Login", "Account");
            }
            else
            {
                return View();
            }
            
        }

        // POST: /Duyuru/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="Duyuru_ID,Duyuru_Tarih,Duyuru_Aciklama")] Duyurular duyurular)
        {
            if (ModelState.IsValid)
            {
                db.Duyurular.Add(duyurular);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(duyurular);
        }

        // GET: /Duyuru/Edit/5
        public ActionResult Edit(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyurular duyurular = db.Duyurular.Find(id);
            if (duyurular == null)
            {
                return HttpNotFound();
            }
            return View(duyurular);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Duyuru/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="Duyuru_ID,Duyuru_Tarih,Duyuru_Aciklama")] Duyurular duyurular)
        {
            /*if (ModelState.IsValid)
            {
                db.Entry(duyurular).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(duyurular);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Duyuru/Delete/5
        public ActionResult Delete(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Duyurular duyurular = db.Duyurular.Find(id);
            if (duyurular == null)
            {
                return HttpNotFound();
            }
            return View(duyurular);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Duyuru/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            /*Duyurular duyurular = db.Duyurular.Find(id);
            db.Duyurular.Remove(duyurular);
            db.SaveChanges();
            return RedirectToAction("Index");*/
            return RedirectToAction("Index", "Home");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

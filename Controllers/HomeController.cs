﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;
namespace HizmetTalep.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            return View();
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Sakarya Üniversitesi Bilgisayar Ve Bilişim Bilimleri Fakültesi Bilgisayar Mühendisliği Bölümü";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = HizmetTalep.Resources.lang.Iletisim;

            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
    }
}
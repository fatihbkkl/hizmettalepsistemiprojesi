﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HizmetTalep.Models;

namespace HizmetTalep.Controllers
{
    public class AdminController : Controller
    {
        private HizmetTalepEntities db = new HizmetTalepEntities();
        

        // GET: /Admin/
        //duyurular,sikayetler bitti.
        public ActionResult Index()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                AdminDuyuru obj=new AdminDuyuru();
                obj.duyuru=db.Duyurular.OrderByDescending(x=>x.Duyuru_ID).Take(3).ToList();
                return View("Index",obj);
            }
            else
            {
                return RedirectToAction("Index", "Home");

            }
        }
        #region Duyurular
        //BİTTİ
        public ActionResult Duyurular()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                return View(db.Duyurular.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult DuyuruSil(int? id)
        {
            /*try
            {*/
                if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
                {
                    db.Duyurular.Remove(db.Duyurular.First(d => d.Duyuru_ID == id));
                    db.SaveChanges();
                    return RedirectToAction("Duyurular", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                
            //}
            /*catch (Exception hata)
            {
                
                throw new Exception("Silerken Hata Oluştu",hata);
            }   */
        }
        public ActionResult DuyuruDuzenle(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Duyurular duyurular = db.Duyurular.Find(id);
                if (duyurular == null)
                {
                    return HttpNotFound();
                }
                return View(duyurular);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DuyuruDuzenle([Bind(Include = "Duyuru_ID,Duyuru_Tarih,Duyuru_Aciklama")] Duyurular duyurular)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(duyurular).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(duyurular);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult DuyuruOlustur()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DuyuruOlustur([Bind(Include="Duyuru_ID,Duyuru_Tarih,Duyuru_Aciklama")] Duyurular duyurular)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                if (ModelState.IsValid)
                {
                    db.Duyurular.Add(duyurular);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(duyurular);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        #endregion//BİTTİ
        #region Sikayetler
        public ActionResult Sikayetler()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                return View(db.Sikayetler.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult SikayetSil(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//sisteme giris yapan admin ise silme işlemini yapar.
                db.Sikayetler.Remove(db.Sikayetler.First(d => d.Sikayet_ID == id));
                db.SaveChanges();
                return RedirectToAction("Sikayetler", "Admin");
            }
            else
            {//sisteme giris yapan admin değilse.
                return RedirectToAction("Index", "Home");
            }
           
        }
        public ActionResult SikayetDuzenle(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Sikayetler sikayetler = db.Sikayetler.Find(id);
                if (sikayetler == null)
                {
                    return HttpNotFound();
                }
                return View(sikayetler);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SikayetDuzenle([Bind(Include = "Sikayet_ID,SikayetYapan_Mail,Sikayet_Tarihi,Sikayet_Aciklama")] Sikayetler sikayetler)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"])==1)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(sikayetler).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(sikayetler);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
           
        }
        public ActionResult SikayetOlustur()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SikayetOlustur([Bind(Include = "Sikayet_ID,SikayetYapan_Mail,Sikayet_Tarihi,Sikayet_Aciklama")] Sikayetler sikayetler)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.Sikayetler.Add(sikayetler);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(sikayetler);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
           
        }
        #endregion//BİTTİ
        #region Calisanlar
        
        public ActionResult Calisanlar()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                return View(db.HizmetVeren.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult CalisanSil(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//sisteme giris yapan admin ise silme işlemini yapar.
                db.HizmetVeren.Remove(db.HizmetVeren.First(d => d.HizmetVeren_ID == id));
                db.SaveChanges();
                return RedirectToAction("Calisanlar", "Admin");
            }
            else
            {//sisteme giris yapan admin değilse.
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult CalisanDuzenle(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//admin ise.
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                HizmetVeren hizmetveren = db.HizmetVeren.Find(id);
                if (hizmetveren == null)
                {
                    return HttpNotFound();
                }
                return View(hizmetveren);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CalisanDuzenle([Bind(Include="HizmetVeren_ID,HizmetVeren_Adi,HizmetVeren_Soyadi,HizmetVeren_Telefon,HizmetVeren_Mail,HizmetVeren_Adres")] HizmetVeren hizmetveren)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(hizmetveren).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(hizmetveren);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult CalisanOlustur()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CalisanOlustur([Bind(Include = "HizmetVeren_ID,HizmetVeren_Adi,HizmetVeren_Soyadi,HizmetVeren_Telefon,HizmetVeren_Mail,HizmetVeren_Adres")] HizmetVeren hizmetveren)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.HizmetVeren.Add(hizmetveren);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(hizmetveren);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
        #region Hizmetler
        public ActionResult Hizmetler()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                return View(db.IstenenHizmet.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult HizmetSil(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//sisteme giris yapan admin ise silme işlemini yapar.
                db.IstenenHizmet.Remove(db.IstenenHizmet.First(d => d.IstenenHizmet_ID == id));
                db.SaveChanges();
                return RedirectToAction("Hizmetler", "Admin");
            }
            else
            {//sisteme giris yapan admin değilse.
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult HizmetDuzenle(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//admin ise.
                 if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
                IstenenHizmet istenenhizmet = db.IstenenHizmet.Find(id);
            if (istenenhizmet == null)
            {
                return HttpNotFound();
            }
                ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi", istenenhizmet.IstenenHizmet_Tipi);
                return View(istenenhizmet);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HizmetDuzenle([Bind(Include="IstenenHizmet_ID,IstenenHizmet_Tipi,HizmetiIsteyen_KisiAdSoyad,HizmetiIsteyen_KisiTelefon,IstenenHizmet_TalepTarihi")] IstenenHizmet istenenhizmet)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(istenenhizmet).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi", istenenhizmet.IstenenHizmet_Tipi);
                return View(istenenhizmet);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult HizmetOlustur()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi");
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HizmetOlustur([Bind(Include="IstenenHizmet_ID,IstenenHizmet_Tipi,HizmetiIsteyen_KisiAdSoyad,HizmetiIsteyen_KisiTelefon,IstenenHizmet_TalepTarihi")] IstenenHizmet istenenhizmet)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.IstenenHizmet.Add(istenenhizmet);
                    db.SaveChanges();
                    return RedirectToAction("Hizmetler", "Admin");
                }

                ViewBag.IstenenHizmet_Tipi = new SelectList(db.HizmetTur_Tipi, "HizmetTur_ID", "HizmetTur_Adi", istenenhizmet.IstenenHizmet_Tipi);
                return View(istenenhizmet);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
        #region Kullanicilar
        public ActionResult Kullanici()
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                var kullanicilar = db.Kullanicilar.Include(k => k.Yetkilendirme);
                return View(kullanicilar.ToList());
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult KullaniciSil(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//sisteme giris yapan admin ise silme işlemini yapar.
                db.Kullanicilar.Remove(db.Kullanicilar.First(d => d.Kullanici_ID == id));
                db.SaveChanges();
                return RedirectToAction("Kullanici", "Admin");
            }
            else
            {//sisteme giris yapan admin değilse.
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult KullaniciDuzenle(int? id)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {//admin ise.
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Kullanicilar kullanicilar = db.Kullanicilar.Find(id);
                if (kullanicilar == null)
                {
                    return HttpNotFound();
                }
                ViewBag.Kullanici_YetkiTipi = new SelectList(db.Yetkilendirme, "Yetki_ID", "Yetki_Adi", kullanicilar.Kullanici_YetkiTipi);
                return View(kullanicilar);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KullaniciDuzenle([Bind(Include="Kullanici_ID,Kullanici_UserName,Kullanici_Password,Kullanici_Adi,Kullanici_Soyadi,Kullanici_Mail,Kullanici_Adres,Kullanici_Telefon,Kullanici_KayitTarihi,Kullanici_YetkiTipi")] Kullanicilar kullanicilar)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.Entry(kullanicilar).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.Kullanici_YetkiTipi = new SelectList(db.Yetkilendirme, "Yetki_ID", "Yetki_Adi", kullanicilar.Kullanici_YetkiTipi);
                return View(kullanicilar);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public ActionResult KullaniciOlustur()   
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                ViewBag.Kullanici_YetkiTipi = new SelectList(db.Yetkilendirme, "Yetki_ID", "Yetki_Adi");
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KullaniciOlustur([Bind(Include = "Kullanici_ID,Kullanici_UserName,Kullanici_Password,Kullanici_Adi,Kullanici_Soyadi,Kullanici_Mail,Kullanici_Adres,Kullanici_Telefon,Kullanici_KayitTarihi,Kullanici_YetkiTipi")] Kullanicilar kullanicilar)
        {
            if (Convert.ToInt32(Session["GirisYapanYetki"]) == 1)
            {
                if (ModelState.IsValid)
                {
                    db.Kullanicilar.Add(kullanicilar);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                ViewBag.Kullanici_YetkiTipi = new SelectList(db.Yetkilendirme, "Yetki_ID", "Yetki_Adi", kullanicilar.Kullanici_YetkiTipi);
                return View(kullanicilar);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
        public ActionResult LogOff()
        {
            Session["Durum"] = null;
            Session["GirisYapanYetki"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
    public class AdminDuyuru
    {
        public List<Duyurular> duyuru { get; set; }
    }
}
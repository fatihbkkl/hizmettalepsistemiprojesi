﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HizmetTalep.Models;

namespace HizmetTalep.Controllers
{
    public class CalisanController : Controller
    {
        private HizmetTalepEntities db = new HizmetTalepEntities();

        // GET: /Calisan/
        public ActionResult Index()
        {
            return View(db.HizmetVeren.ToList());
        }

        // GET: /Calisan/Details/5
        public ActionResult Details(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HizmetVeren hizmetveren = db.HizmetVeren.Find(id);
            if (hizmetveren == null)
            {
                return HttpNotFound();
            }
            return View(hizmetveren);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Calisan/Create
        public ActionResult Create()
        {
            return RedirectToAction("Index", "Home");
        }

        // POST: /Calisan/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="HizmetVeren_ID,HizmetVeren_Adi,HizmetVeren_Soyadi,HizmetVeren_Telefon,HizmetVeren_Mail,HizmetVeren_Adres")] HizmetVeren hizmetveren)
        {
            /*if (ModelState.IsValid)
            {
                db.HizmetVeren.Add(hizmetveren);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hizmetveren);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Calisan/Edit/5
        public ActionResult Edit(int? id)
        {
            /*if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HizmetVeren hizmetveren = db.HizmetVeren.Find(id);
            if (hizmetveren == null)
            {
                return HttpNotFound();
            }
            return View(hizmetveren);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Calisan/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="HizmetVeren_ID,HizmetVeren_Adi,HizmetVeren_Soyadi,HizmetVeren_Telefon,HizmetVeren_Mail,HizmetVeren_Adres")] HizmetVeren hizmetveren)
        {
            /*if (ModelState.IsValid)
            {
                db.Entry(hizmetveren).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hizmetveren);*/
            return RedirectToAction("Index", "Home");
        }

        // GET: /Calisan/Delete/5
        public ActionResult Delete(int? id)
        {
           /* if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            HizmetVeren hizmetveren = db.HizmetVeren.Find(id);
            if (hizmetveren == null)
            {
                return HttpNotFound();
            }
            return View(hizmetveren);*/
            return RedirectToAction("Index", "Home");
        }

        // POST: /Calisan/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
           /* HizmetVeren hizmetveren = db.HizmetVeren.Find(id);
            db.HizmetVeren.Remove(hizmetveren);
            db.SaveChanges();*/
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
